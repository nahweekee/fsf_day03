//load express after you have done npm install --save express
var express = require("express");

//Create an application
var app = express();

//Use this directory as the document route
app.use( express.static(__dirname + "/public") );

//Start our web server on port 3000
app.listen(3000, function() {
    console.info("My web server has started on port 3000");
    console.info("Document route is at " + __dirname + "/public");
    console.info("Copyright")
});